## discourse-blender-id

This plugin allows you to use Blender ID as authentication for Discourse.

Plugin also supports Blender ID community badges.

## How does it work?

This is how a plugin [is installed](https://meta.discourse.org/t/install-plugins-in-discourse/19157).


## Usage

First, set up your Discourse application remotely on your OAuth2 provider.
It will require a **Redirect URI** which should be:

`https://DISCOURSE_HOST/auth/blender_id/callback`

Replace `DISCOURSE_HOST` with the approriate value. The OAuth2 provider should
supply you with a client ID and secret, as well as a couple of URLs.

Visit your **Admin** > **Settings** > **Login** and fill in the basic
configuration for the OAuth2 provider:

* `blender_id_enabled` - check this off to enable the feature

* `blender_id_client_id` - the client ID from your provider

* `blender_id_client_secret` - the client secret from your provider


The plugin will also start a background job, which will run through the
existing OAuth credentials and try to fetch badges for the users.
The job will run every 30 minues.

### License

MIT
