# frozen_string_literal: true

class MigrateBlenderIdUserInfo < ActiveRecord::Migration[6.1]
  def up
    execute <<~SQL
    INSERT INTO user_associated_accounts (
      user_id,
      provider_name,
      provider_uid,
      credentials,
      created_at,
      updated_at
    ) SELECT
      DISTINCT ON (user_id)
      user_id,
      'blender_id',
      provider_uid,
      credentials,
      u.created_at,
      u.updated_at
    FROM (
        SELECT
          (value::JSON ->> 'user_id')::int as user_id,
          (value::JSON ->> 'oauth_user_id')::int as provider_uid,
          CASE
            WHEN (value::JSON ->> 'credentials') IS NULL THEN '{}'
            ELSE (value::JSON ->> 'credentials')::JSON
          END as credentials
       FROM plugin_store_rows
       WHERE plugin_name = 'blender_id' AND key LIKE 'blender_id_user_%'
       ORDER BY provider_uid DESC
    ) as tmp
    JOIN users as u ON (tmp.user_id = u.id)
    SQL
  end

  def down
    execute <<~SQL
    DELETE FROM user_associated_accounts
    WHERE provider_name = 'blender_id'
    SQL
  end
end
