require_relative "../../blender_id_utils"

module Jobs
  class BlenderIdUpdateBadges < ::Jobs::Base
    sidekiq_options retry: false

    include BlenderIdUtils

    def execute(args)
      log_warn("called with #{args}")

      provider_uid = args['id']
      log_debug("provider_uid: '#{provider_uid}'")
      if provider_uid.blank?
        log_warn("No Blender ID given, skipping")
        return
      end

      row = UserAssociatedAccount.where("provider_name = ? AND (credentials->'token') is not null and provider_uid = ?", 'blender_id', provider_uid.to_s).first
      if row.nil?
        log_warn("No token found for Blender ID '#{provider_uid}', skipping")
        return
      end

      user = User.where(id: row.user_id).first
      if user.nil?
        log_warn("No User found for user_id '#{row.user_id}', skipping")
        return
      end

      user_badges = args['badges']
      log_warn("Updating badges for User: #{user.id} with #{user_badges}")
      update_user_badges(user_badges, user)
    end
  end
end
