module BlenderIdUtils
  extend self

  def log_debug(msg)
    prev_caller = caller_locations(1,1)[0].label
    Rails.logger.warn("[Blender ID Debug #{prev_caller} #{self.class.name}] #{msg}") if SiteSetting.blender_id_debug_auth
  end

  def log_warn(msg)
    prev_caller = caller_locations(1,1)[0].label
    Rails.logger.warn("[Blender ID #{prev_caller} #{self.class.name}]: #{msg}")
  end

  def get_blender_id_badges
    # Get the existing badge names from Plugin Store
    badges = ::PluginStore.get("blender_id", "blender_id_badges")
    if not badges
      return Array.new()
    end
    return badges
  end

  def basic_auth_header
    "Basic " + Base64.strict_encode64("#{SiteSetting.blender_id_client_id}:#{SiteSetting.blender_id_client_secret}")
  end

  def walk_path(fragment, segments)
    first_seg = segments[0]
    return if first_seg.blank? || fragment.blank?
    return nil unless fragment.is_a?(Hash) || fragment.is_a?(Array)
    if fragment.is_a?(Hash)
      deref = fragment[first_seg] || fragment[first_seg.to_sym]
    else
      deref = fragment[0] # Take just the first array for now, maybe later we can teach it to walk the array if we need to
    end

    return (deref.blank? || segments.size == 1) ? deref : walk_path(deref, segments[1..-1])
  end

  def json_walk(result, user_json, prop)
    path = SiteSetting.send("blender_id_json_#{prop}_path")
    if path.present?
      segments = path.split('.')
      val = walk_path(user_json, segments)
      result[prop] = val if val.present?
    end
  end

  def query_api_endpoint(token, endpoint)
    # Fetch JSON info from an api endpoint (this is the place where we talk to Blender ID)
    api_url = "#{SiteSetting.blender_id_url}api/#{endpoint}"
    log_debug("api_url: GET #{api_url}, #{token}")
    bearer_token = "Bearer #{token}"
    json_response = URI.open(api_url, 'Authorization' => bearer_token).read
    return JSON.parse(json_response)
  end

  def fetch_user_details(token)
    user_json = query_api_endpoint(token, "me")

    log_debug("user_json: #{user_json}")
    result = {}
    if user_json.present?
      json_walk(result, user_json, :user_id)
      json_walk(result, user_json, :username)
      json_walk(result, user_json, :name)
      json_walk(result, user_json, :email)
      json_walk(result, user_json, :avatar)
    end
    result['badges'] = user_json['badges']
    log_debug("fetch_user_details #{result}, #{user_json}")
    result
  end

  def update_user_badges(badges, user)
    # Add or remove Blender ID badges
    
    badge_names_incoming = Set.new()
    badges.each do |key, value|
      log_debug("processing badge: #{key}")
      # Make sure the badge exists in Discourse
      # TODO(fsiddi): Update the badge if something changed (e.g. image or description)
      unless b = Badge.find_by(name: value['label'])
        image = value.has_key?('image') ? value['image'] : nil
        b = Badge.create!(name: value['label'],
          description: value['label'],
          # TODO(anna,fsiddi): figure out a working way of creating badges with an image
          # because this doesn't actually work.
          # image: image,
          badge_type_id: 1)
      end
      # Assign the badge (ignoring if the user already has it)
      BadgeGranter.grant(b, user)
      # Add to list for comparing with badge_names_all later
      badge_names_incoming << value['label']
    end

    badge_names_all = get_blender_id_badges.to_set

    # Combine all the exsiting badges with the incoming one
    # This is meant to automatically extend the list of existing badges
    badge_names_all_updated = badge_names_all + badge_names_incoming
    ::PluginStore.set("blender_id", "blender_id_badges", badge_names_all_updated.to_a)

    # Find and remove old badges
    to_remove_badges = badge_names_all_updated - badge_names_incoming
    to_remove_badges.each { |badge_name|
      b = Badge.find_by(name: badge_name)
      if b
        ub = UserBadge.find_by(badge_id: b.id, user_id: user.id)
        if ub
          BadgeGranter.revoke(ub)
        end
      end
    }
  end
end
