=begin
Handle account modifications sent by Blender ID.

Payload is expected to be a JSON containing (at least) the following keys:

  {
    "id": 99999999,
    "date_deletion_requested": null,
    ...
    "badges": {
      "cloud_subscriber": {
        "image": "https://example.com/media/badges/badge_cloud.png",
        "image_height": 256,
        "image_width": 256,
        "label": "Blender Studio"
      },
      "devfund_bronze": {
        "image": "https://example.com/media/badges/badge_devfund_bronze.png",
        "image_height": 256,
        "image_width": 256,
        "label": "Bronze Development Fund supporter"
      }
    }
  }

=end

require 'json'


class BlenderIdWebhookController < ApplicationController

  # Skip CSRF check and allow calling this endpoing via regular HTTP.
  skip_before_action :verify_authenticity_token, :check_xhr

  WEBHOOK_MAX_BODY_SIZE = 1024 * 10  # 10 kB is large enough

  require_relative "../jobs/regular/blender_id_update_badges"

  def update_badges
    log_debug("called #{self.class.name}##{__method__}")
    hmac_secret = SiteSetting.blender_id_webhook_secret
    if hmac_secret == ''
      log_debug("webhook secret isn't set, skipping")
      return render body: nil, status: 200
    end

    # Check the content type
    if request.headers["Content-Type"] != 'application/json'
      log_warn("unexpected content type: \"#{request.headers['Content-Type']}\"")
      return render body: nil, status: :bad_request
    end

    # Check the length of the body
    content_length = request.headers['CONTENT_LENGTH'].to_i
    if content_length > WEBHOOK_MAX_BODY_SIZE
      log_warn("request body too large \"#{content_length}\"")
      return render body: nil, status: 413
    end
    body = request.raw_post()
    if body.size != content_length
      log_warn("Content-Length \"#{content_length}\" does not match content size #{body.size}")
      return render body: nil, status: :bad_request
    end

    # Validate the request
    req_hmac = request.headers['X-Webhook-HMAC']
    our_hmac = OpenSSL::HMAC.hexdigest('sha256', hmac_secret, body)
    if req_hmac != our_hmac
      log_warn("Invalid HMAC #{req_hmac}, expected #{our_hmac}")
      return render body: nil, status: :bad_request
    end

    payload = JSON.parse(body)
    log_debug("payload: #{payload}")
    provider_uid = payload['id']
    if provider_uid.blank?
      log_warn("payload contains no Blender ID, skipping")
      return render body: nil, status: :bad_request
    end
    if payload['date_deletion_requested']
      log_warn("deletion was requested for Blender ID #{provider_uid}, ignoring this update")
      return render json: {}, status: 200
    end
    Jobs.enqueue(:blender_id_update_badges, payload)
    render json: {}, status: 202
  end

  private
    def log_debug(msg)
      Rails.logger.warn("[Blender ID Webhook Debug]: #{msg}") if SiteSetting.blender_id_debug_auth
    end

    def log_warn(msg)
      Rails.logger.warn("[Blender ID Webhook]: #{msg}")
    end
end
