# name: discourse-blender-id
# about: Blender ID OAuth Plugin
# version: 0.5
# authors: Francesco Siddi, Anna Sirota
# url: https://projects.blender.org/infrastructure/discourse-blender-id/

enabled_site_setting :blender_id_enabled
require_relative "app/blender_id_utils"


class BlenderIdStrategy < OmniAuth::Strategies::OAuth2
  include BlenderIdUtils
  option :name, "blender_id"

  def raw_info
    log_debug("#{access_token.to_hash}")
    @raw_info ||= fetch_user_details(access_token.to_hash[:access_token])
  end

  uid do
    raw_info[:user_id]
  end

  info do
    {
      name: raw_info[:name],
      username: raw_info[:username],
      email: raw_info[:email],
      image: raw_info[:avatar],
      badges: raw_info['badges']
    }
  end

  def callback_url
    Discourse.base_url_no_prefix + script_name + callback_path
  end
end

class BlenderIdAuthenticator < Auth::ManagedAuthenticator
  include BlenderIdUtils

  def name
    "blender_id"
  end

  def register_middleware(omniauth)
    omniauth.provider BlenderIdStrategy,
                      setup: lambda { |env|
                        opts = env['omniauth.strategy'].options
                        opts[:client_id] = SiteSetting.blender_id_client_id
                        opts[:client_secret] = SiteSetting.blender_id_client_secret
                        opts[:provider_ignores_state] = false
                        opts[:client_options] = {
                          authorize_url: "#{SiteSetting.blender_id_url}oauth/authorize",
                          token_url: "#{SiteSetting.blender_id_url}oauth/token",
                          token_method: 'post'.to_sym
                        }
                        opts[:authorize_options] = SiteSetting.blender_id_authorize_options.split("|").map(&:to_sym)
                        opts[:token_params] = { headers: { 'Authorization' => basic_auth_header } }

                        unless SiteSetting.blender_id_scope.blank?
                          opts[:scope] = SiteSetting.blender_id_scope
                        end
                      }
  end

  def enabled?
    SiteSetting.blender_id_enabled
  end

  def after_authenticate(auth_token, existing_account: nil)
    result = super
    log_debug("info: #{auth_token[:info]}")
    if SiteSetting.blender_id_update_badges_on_login
      provider_uid = auth_token.uid
      log_warn("will update badges for Blender ID '#{provider_uid}'")
      params = {id: auth_token.uid, badges: auth_token[:info]['badges']}
      Jobs.enqueue(:blender_id_update_badges, params)
    end
    return result
  end

end

after_initialize do
  require_relative 'app/controllers/blender_id_webhook_controller'

  Discourse::Application.routes.append do
    post '/webhooks/blender-id/user-modified/' => 'blender_id_webhook#update_badges'
  end
end

auth_provider title_setting: "blender_id_button_title",
              pretty_name: "Blender ID",
              authenticator: BlenderIdAuthenticator.new()


register_css <<CSS

  button.btn-social.blender_id {
    background-color: #6d6d6d;
  }

CSS
